import { Component, ViewChild, OnInit } from '@angular/core';
import { ModalController, ToastController, Platform } from '@ionic/angular';
import { Camera } from '@ionic-native/camera/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Keyboard } from '@ionic-native/keyboard/ngx';

import { LanguagePickerPage } from '../modals/language-picker/language-picker.page';
import { LanguageSetup } from '../models/language-setup';
import { LanguagePickerConstants } from '../constants/modals';
import { LanguagesService } from '../services/languages.service';
import { Translation } from '../models/translation';
import { TranslationService } from '../services/translation.service';
import { ErrorsConstants } from '../constants/errors';
import { ToastOptionsFactory } from '../constants/toast-options-factory';
import { PicturePickerPage } from '../modals/picture-picker/picture-picker.page';
import { OcrService } from '../services/ocr.service';
import { MemosService } from '../services/memos.service';
import { ConfigConstants } from '../constants/config';
import { Coords } from '../models/coords';
import { RecentMemos } from '../models/view/recent-memos';
import { SettingsService } from '../services/settings.service';
import { Content } from '@ionic/angular';
import { HomeConstants } from '../constants/home';

declare var google;
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss']
})
export class HomePage implements OnInit {
  private static fakePhotoPath = 'assets/examples/photo1.jpeg';

  @ViewChild('recents') recentsElement: Content;

  public languagesMap: Map<string, string> = new Map<string, string>();
  public languageSetup: LanguageSetup = {} as LanguageSetup;
  public isAddressRecognition = false;
  public typedText = '';
  public isRecognizing = false;
  public translateTimeOut: any;
  public scrollRecentsTimeOut: any;
  public isRecentMemosLoaded = true;
  public recentMemosModel: RecentMemos = null;

  constructor(
    private modalController: ModalController,
    private platform: Platform,
    private languagesService: LanguagesService,
    private translationService: TranslationService,
    private ocrService: OcrService,
    private memosService: MemosService,
    private settingsService: SettingsService,
    private camera: Camera,
    private geolocation: Geolocation,
    private keyboard: Keyboard,
    private toastController: ToastController) { }

    async ngOnInit() {
    this.languagesService.getCachedAsMap().subscribe(languages => {
      this.languagesMap = languages;
      this.languageSetup = new LanguageSetup(ConfigConstants.DefaultFromLang, ConfigConstants.DefaultToLang);
    });
    this.keyboard.onKeyboardShow().subscribe(async () => { this.delayScrollToBottom(); });
    this.memosService.trackingSubject().subscribe(async () => {
      await this.loadRecents();
      this.isRecentMemosLoaded = true;
      this.delayScrollToBottom();
    });
    this.delayScrollToBottom();
  }

  public get isDevice(): boolean {
    return this.platform.is('android');
  }

  public get from(): string {
    return this.languagesMap.get(this.languageSetup.from);
  }

  public get to(): string {
    return this.languagesMap.get(this.languageSetup.to);
  }

  get isSavingGeolocationEnabled(): boolean {
    return this.settingsService.saveGeolocation;
  }

  public async selectFrom() {
    const modal = await this.modalController.create({
      component: LanguagePickerPage,
      componentProps: {
        selected: this.languageSetup.from,
        includeAuto: false,
        title: LanguagePickerConstants.fromTitle
      }
    });
    await modal.present();
    const { data } = await modal.onDidDismiss();
    this.languageSetup.from = data;
  }

  public async selectTo() {
    const modal = await this.modalController.create({
      component: LanguagePickerPage,
      componentProps: {
        selected: this.languageSetup.to,
        includeAuto: false,
        title: LanguagePickerConstants.toTitle
      }
    });
    await modal.present();
    const { data } = await modal.onDidDismiss();
    this.languageSetup.to = data;
  }

  public swap() {
    const tempFrom = this.languageSetup.from;
    if (tempFrom === 'auto') { return; }
    this.languageSetup.from = this.languageSetup.to;
    this.languageSetup.to = tempFrom;
  }

  public async translateFromPhoto() {
    try {
      const image = await this.takePhoto();
      if (!image) {
        return;
      }

      this.isRecognizing = true;
      const result = await this.ocrService.recognizeText(image, this.languageSetup.from, v => {
        console.log(v);
        if (v.status === 1) {
          this.isRecognizing = false;
        }
      });
      this.typedText = result.text;
      await this.translate(this.typedText);
    } catch (err) {
      console.log(err);
    } finally {
      this.isRecognizing = false;
    }
  }

  public delayTranslate(text: string) {
    if (this.translateTimeOut) {
      clearTimeout(this.translateTimeOut);
    }
    this.translateTimeOut = setTimeout(async () => await this.translate(text), 900);
  }

  public async translate(text: string) {
    if (!text.trim().length) {
      return;
    }
    this.isRecentMemosLoaded = false;

    this.translationService.getTranslation(text, this.languageSetup)
      .subscribe(async result => {
        let location: Coords = null;
        let locationName: string  = null;

        let recognizedLocation = null;
        if (this.isAddressRecognition) {
          try {
            recognizedLocation = await this.tryGetLocationByAddress(text);
          } catch (e) {
            console.log(e);
          }
        }

        if (this.isSavingGeolocationEnabled && !this.isAddressRecognition) {
          console.log('getting location');
          location = (await this.tryGetLocation()) || ConfigConstants.DefaultLocation;
          locationName = await this.tryGetLocationName(location);
        }

        const translation = {
          creationDate: new Date(),
          text: result.text,
          translatedText: result.translatedText,
          setup: Object.assign({}, result.setup),
          location: recognizedLocation ? recognizedLocation.coords : location,
          locationName: recognizedLocation ? recognizedLocation.name : locationName,
          isAddress: this.isAddressRecognition
        } as Translation;
        try {
        await this.memosService.addIfNotExist(translation);
        } finally {
          this.isRecentMemosLoaded = true;
          await this.delayScrollToBottom();
        }
      },
        async error => {
          console.log(error);
          const toast = await this.toastController.create(
            ToastOptionsFactory.error(ErrorsConstants.TranslateServiceError));
          toast.present();
          this.isRecentMemosLoaded = true;
        });
  }

  private tryGetLocationByAddress(queryText: string): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      const geocoder = new google.maps.Geocoder;
      geocoder.geocode({ address: queryText }, (results: any[], status) => {
        if (status === 'OK') {
          if (results.length) {
            const location = {
              name: results[0].formatted_address,
              coords: {
                lat: results[0].geometry.location.lat(),
                lng: results[0].geometry.location.lng()
              }
            };
            resolve(location);
            return location;
          }
        }
        reject(status);
      });
    });
  }

  private async tryGetLocation(): Promise<Coords> {
    if (this.isDevice) {
      try {
        const geo = await this.geolocation.getCurrentPosition(ConfigConstants.GeolocationOptions);
        return new Coords(geo.coords.latitude + Math.random(), geo.coords.longitude + Math.random());
      } catch (error) {
        const toast = await this.toastController.create(
          ToastOptionsFactory.error(ErrorsConstants.GeolocationError));
        toast.present();
        console.log(error);
      }
    }
    return null;
  }

  private async tryGetLocationName(coords: Coords): Promise<string> {
    return new Promise<string>((resolve, reject) => {
      const geocoder = new google.maps.Geocoder;
      geocoder.geocode({ location: coords }, function (results, status) {
        if (status === 'OK') {
          if (results[0]) {
            resolve(results[0].formatted_address);
            return results[0].formatted_address;
          }
        }
        reject(status);
      });
    });
  }

  private async takePhoto() {
    let imageData;
    if (!this.isDevice) {
      imageData = HomePage.fakePhotoPath;
    } else {
      try {
        imageData = await this.camera.getPicture(ConfigConstants.CameraOptions);
      } catch (message) {
        const toast = await this.toastController.create(ToastOptionsFactory.error(message));
        toast.present();
        return;
      }
    }
    return this.getCroppedPicture(imageData);
  }

  private async getCroppedPicture(imageData) {
    const modal = await this.modalController.create({
      component: PicturePickerPage,
      componentProps: {
        image: 'data:image/jpeg;base64,' + imageData,
        imageUrl: this.isDevice ? null : imageData
      }
    });
    await modal.present();
    const { data } = await modal.onDidDismiss();
    return data.croppedImage;
  }

  private async loadRecents() {
    this.recentMemosModel = await this.memosService.getRecents().toPromise();
  }

  private async delayScrollToBottom() {
    if (this.scrollRecentsTimeOut) {
      clearTimeout(this.scrollRecentsTimeOut);
    }
    this.scrollRecentsTimeOut = setTimeout(async () => await this.scrollToBottom(), 50);
  }

  private async scrollToBottom() {
    console.log(this.recentsElement);
    return this.recentsElement.scrollToBottom(0);
  }
}
