import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { AngularCropperjsModule } from 'angular-cropperjs';

import { IonicModule } from '@ionic/angular';

import { PicturePickerPage } from './picture-picker.page';

const routes: Routes = [
  {
    path: '',
    component: PicturePickerPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    AngularCropperjsModule
  ],
  declarations: [PicturePickerPage]
})
export class PicturePickerPageModule {}
