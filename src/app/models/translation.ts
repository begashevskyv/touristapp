import { LanguageSetup } from './language-setup';
import { Coords } from './coords';

export class Translation {
    constructor(
        public creationDate: Date,
        public setup: LanguageSetup,
        public location: Coords,
        public locationName: string,
        public text: string,
        public translatedText: string,
        public isAddress: boolean) {
    }
}
