import { Translation } from '../translation';
export class RecentMemos {
    constructor(
        public recents: Translation[],
        public last: Translation) {
    }
}
