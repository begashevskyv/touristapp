import { Component, OnInit, AfterViewInit, OnChanges } from '@angular/core';
import { MemosService } from '../services/memos.service';
import { Translation } from '../models/translation';

@Component({
  selector: 'app-memos',
  templateUrl: 'memos.page.html',
  styleUrls: ['memos.page.scss']
})
export class MemosPage implements OnInit {


  public translations: Translation[] = null;
  public grouped: Map<string, Translation[]> = null;
  public selectedGroups: Map<string, boolean> = null;

  public searchTimeOut: any;
  public isAddressType: boolean;

  constructor(private memosService: MemosService) { }

  async ngOnInit() {
    await this.loadMemos();
    this.memosService.trackingSubject().subscribe(async r => {
      await this.loadMemos();
    });
  }

  get translationList() {
    return this.translations;
  }

  get selectedGroupList() {
    return this.selectedGroups;
  }

  get noResults(): boolean {
    return (!this.translations || !this.translations.length) && !this.noAnyMemos;
  }

  get noAnyMemos(): boolean {
    return !this.grouped || !this.grouped.size;
  }

  selectGroup(key) {
    const value = this.selectedGroups.get(key);
    this.selectedGroups.set(key, !value);
    this.applyGroupFilter();
  }

  delaySearch(text: string) {
    if (this.searchTimeOut) {
      clearTimeout(this.searchTimeOut);
    }
    this.searchTimeOut = setTimeout(() => this.applySearchFilter(text), 300);
  }

  async applyTypeFilter(isAddressType: boolean) {
    this.isAddressType = isAddressType;
    await this.loadMemos();
  }

  private async loadMemos() {
    const memos = await this.memosService.getAll(this.isAddressType).toPromise();
    this.translations = memos;
    this.memosService.getGroupedByLanguageSetup(this.isAddressType).subscribe(r => {
      this.grouped = r;
      const keys = Array.from(this.grouped.keys());
      this.selectedGroups = new Map<string, boolean>();
      keys.forEach(k => {
        this.selectedGroups.set(k, false);
      });
    });
  }

  private async applySearchFilter(text: string) {
    const lower = text.toLowerCase().trim();
    await this.applyGroupFilter();
    this.translations = this.translations
      .filter(t =>
        (t.locationName && t.locationName.toLowerCase().includes(lower))
        || t.text.toLowerCase().includes(lower)
        || t.translatedText.toLowerCase().includes(lower));
  }

  private async applyGroupFilter(): Promise<boolean> {
    return new Promise<boolean>(async (resolve) => {
      let filtered = [];
      this.selectedGroups.forEach((v, k) => {
        if (v) {
          filtered = filtered.concat(this.grouped.get(k));
        }
      });
      if (!filtered.length) {
        this.translations = await this.memosService.getAll(this.isAddressType).toPromise();
        resolve(false);
        return;
      }
      this.translations = filtered;
      resolve(true);
    });
  }
}
