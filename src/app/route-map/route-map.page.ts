import { Component, OnInit, Input } from '@angular/core';
import { Platform, ToastController } from '@ionic/angular';
import { ErrorsConstants } from '../constants/errors';
import { ToastOptionsFactory } from '../constants/toast-options-factory';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { ConfigConstants } from '../constants/config';
import { RouteMapNavigationService } from '../services/navigation/route-map-navigation.service';
import { RouteTypesEnum } from '../constants/route-map';

declare var google;

@Component({
  selector: 'app-route-map',
  templateUrl: './route-map.page.html',
  styleUrls: ['./route-map.page.scss'],
})
export class RouteMapPage implements OnInit {
  @Input()
  destination = ConfigConstants.DefaultLocation;

  location = ConfigConstants.DefaultLocation;
  isMapLoaded = false;
  selectedType: RouteTypesEnum = RouteTypesEnum.Drive;
  types: { drive: string, walk: string } = { drive: RouteTypesEnum.Drive, walk: RouteTypesEnum.Walk };
  constructor(
    private routeMapNav: RouteMapNavigationService,
    private platform: Platform,
    private geolocation: Geolocation,
    private toastController: ToastController) {
  }

  async ngOnInit() {
    this.destination = this.routeMapNav.destination;
    await this.platform.ready();
    await this.buildRouteFromCurrentLocation();
  }

  public async buildRouteFromCurrentLocation() {
    try {
      const geo = await this.geolocation.getCurrentPosition(ConfigConstants.GeolocationOptions);
      this.location = { lat: geo.coords.latitude, lng: geo.coords.longitude };
    } catch (error) {
      const toast = await this.toastController.create(
        ToastOptionsFactory.error(ErrorsConstants.GeolocationError));
      toast.present();
      console.log(error);
    }
    this.isMapLoaded = false;
    try {
      await this.calculateAndDisplayRoute();
    } catch (error) {
      const toast = await this.toastController.create(
        ToastOptionsFactory.error(ErrorsConstants.GoogleMapError));
      toast.present();
      console.log(error);
    } finally {
      this.isMapLoaded = true;
    }
  }

  public calculateAndDisplayRoute() {
    const there = this;
    return new Promise((resolve, reject) => {
      const directionsService = new google.maps.DirectionsService;
      const directionsDisplay = new google.maps.DirectionsRenderer;
      const map = new google.maps.Map(document.getElementById('map'), {
        zoom: 7,
        center: there.location
      });
      directionsDisplay.setMap(map);
      try {
        directionsService.route({
          origin: there.location,
          destination: there.destination,
          travelMode: there.selectedType
        }, (response, status) => {
          if (status === 'OK') {
            directionsDisplay.setDirections(response);
            resolve({ response, status });
            return;
          }
          reject(status);
        });
      } catch (error) {
        reject(error);
      }
    });
  }

  public async selectType(type: RouteTypesEnum) {
    this.selectedType = type;
    await this.buildRouteFromCurrentLocation();
  }
}
