import { Injectable } from '@angular/core';
import Tesseract from 'tesseract.js';

@Injectable({
  providedIn: 'root'
})
export class OcrService {
  static host = `${window.location.protocol}//${window.location.hostname}:${window.location.port}`;
  private readonly tesseract;
  constructor() {
    this.tesseract = Tesseract.create({
      workerPath:  `${OcrService.host}/assets/lib/worker.js`,
      langPath: `${OcrService.host}/assets/lib/lang/tesseract.js-`,
      corePath: `${OcrService.host}/assets/lib/tesseract.js`,
    });
  }

  public recognizeText(image: any, lang: string, progress: (v: any) => void): Promise<any> {
    const tesseractConfig = {
      lang: lang
    };
    return new Promise<any>((resolve, reject) => {
      this.tesseract.recognize(image, tesseractConfig)
        .progress(p => progress(p))
        .catch((err) => reject(err))
        .then((result) => resolve(result));
    });
  }
}
