import { Injectable } from '@angular/core';
import { Coords } from '../../models/coords';

@Injectable({
  providedIn: 'root'
})
export class RouteMapNavigationService {
  public destination: Coords;

  constructor() { }
}
