import { Injectable } from '@angular/core';
import { Translation } from '../models/translation';
import { Observable, from, BehaviorSubject } from 'rxjs';
import { RecentMemos } from '../models/view/recent-memos';
import { StorageKeyConstants } from '../constants/storageKeys';
import { map } from 'rxjs/operators';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class MemosService {

  private memosSubject = new BehaviorSubject([]);

  constructor(private storage: Storage) { }

  getGroupedByLanguageSetup(isAddressType: boolean): Observable<Map<string, Translation[]>> {
    return this.getAll(isAddressType)
      .pipe(map(translations =>
        this.getGrouped<string>(
          translations,
          (t: Translation) => `${t.setup.from}-${t.setup.to}`)));
  }

  getAll(isAddressType?): Observable<Translation[]> {
    return from(this.storage.get(StorageKeyConstants.translations))
      .pipe(map((translations: Translation[]) => {
        let sorted = translations || [] as Translation[];
        if (isAddressType !== undefined) {
          sorted = sorted.filter(s =>
            s.isAddress === (
              isAddressType === 'true'
              || (typeof isAddressType === 'boolean' && isAddressType === true)));
        }
        if (sorted) {
          sorted.sort((a, b) => {
            let aTicks = 0;
            let bTicks = 0;
            aTicks = a ? new Date(a.creationDate).getTime() : 0;
            bTicks = b ? new Date(b.creationDate).getTime() : 0;
            return bTicks - aTicks;
          });
        }
        return sorted;
      }));
  }

  getRecents(): Observable<RecentMemos> {
    return this.getAll()
      .pipe(map(translations => {
        return translations.length
          ? new RecentMemos(translations.slice(1, 3).reverse(), translations[0])
          : null;
      }));
  }

  async addIfNotExist(translation: Translation): Promise<any> {
    console.log(translation);
    const translations = await this.getAll().toPromise();
    if ((translations.some(t =>
      t.text.toLowerCase() === translation.text.toLocaleLowerCase()
      && t.setup.from === translation.setup.from
      && t.setup.to === translation.setup.to
      && t.isAddress === translation.isAddress)) || this.isEmpty(translation)) {
      return false;
    }
    translations.push(translation);

    const setted = await this.storage.set(StorageKeyConstants.translations, translations);
    this.memosSubject.next(setted);
    return setted;
  }

  async any(): Promise<boolean> {
    const memos = await this.getAll().toPromise();
    return memos.some(() => true);
  }

  async clear() {
    const cleared = await this.storage.set(StorageKeyConstants.translations, []);
    this.memosSubject.next([]);
    return cleared;
  }

  trackingSubject(): BehaviorSubject<Translation[]> {
    return this.memosSubject;
  }

  private getGrouped<T>(translations: Translation[], keyGetter: (t: Translation) => T) {
    const groupedMap = new Map();
    translations.forEach((item) => {
      const key = keyGetter(item);
      const collection = groupedMap.get(key);
      if (!collection) {
        groupedMap.set(key, [item]);
      } else {
        collection.push(item);
      }
    });
    return groupedMap;
  }

  private isEmpty(translation: Translation) {
    return translation.text.trim() === '';
  }
}
