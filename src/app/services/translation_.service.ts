import { Injectable } from '@angular/core';
import { LanguageSetup } from '../models/language-setup';
import { from, Observable } from 'rxjs';
import { HTTP as Http } from '@ionic-native/http/ngx';
import { ConfigConstants } from '../constants/config';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import * as querystring from 'querystring';

@Injectable({
  providedIn: 'root'
})
export class Translation_Service {

  constructor(private http: Http) { }

  public getTranslation(text: string, setup: LanguageSetup): Observable<any> {
    const params = {
      client: 'gtx',
      q: text, // text to be translated
      sl: setup.from, // language from
      tl: setup.to, // language to
      dj: '1',
      tk: this.getToken(text),
      dt: ['qc', 'bd', 'rm',  't']
    };
    var queryParams = querystring.stringify(params);
    console.log(queryParams);
    var dstLangIso = setup.to;
    return from(this.http.get(`${ConfigConstants.GoogleApi}?${queryParams}`, {}, {}))
      .pipe(map((res: any) => this.mapResult(res, dstLangIso)));
  }

  private yf(a, b) {
    for (var c = 0; c < b.length - 2; c += 3) {
      var d: any = b[c + 2]
        , d: any = "a" <= d ? d.charCodeAt(0) - 87 : Number(d)
        , d: any = "+" == b[c + 1] ? a >>> d : a << d;
      a = "+" == b[c] ? a + d & 4294967295 : a ^ d;
    }
    return a;
  }

  private getToken(a) {
    var d = new Array(a.length);

    for (var f = 0, e = 0; f < a.length; ++f) {
      var g = a.charCodeAt(f);
      128 > g ? d[e++] = g : (2048 > g ? d[e++] = g >> 6 | 192 : (d[e++] = g >> 12 | 224,
        d[e++] = g >> 6 & 63 | 128),
        d[e++] = g & 63 | 128);
    }

    var b = 0;
    a = 0;

    for (e = 0; e < d.length; e++)
      a += d[e],
        a = this.yf(a, "+-a^+6");
    a = this.yf(a, "+-3^+b+-f");
    0 > a && (a = (a & 2147483647) + 2147483648);
    a %= 1E6;
    return a.toString() + "." + (a ^ b);
  }

  private mapResult(res, dstLangIso) {
    var result = {
      text: '',
      language: {
        iso: ''
      },
      from: {
        language: {
          didYouMean: false,
          iso: ''
        },
        text: {
          autoCorrected: false,
          value: '',
          didYouMean: false
        }
      },
      raw: ''
    };
    var parsed = res.data.replace(/(\,\,)/g, ',"",').replace(/\[\,/g, '["",').replace(/\,\]/g, ',""]');
    var body = JSON.parse(parsed);
    
    result.text = body.sentences[0].trans;
    result.from.language.iso = body.src;
    result.language.iso = dstLangIso;
    result.from.text.value = body.sentences[0].orig;
    return result;
  }
}
