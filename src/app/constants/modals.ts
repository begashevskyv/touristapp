export enum LanguagePickerConstants {
    fromTitle = 'Select source language',
    toTitle = 'Select destination language'
}
