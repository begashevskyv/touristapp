import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', loadChildren: './tabs/tabs.module#TabsPageModule' },
  { path: 'LanguagePicker', loadChildren: './modals/language-picker/language-picker.module#LanguagePickerPageModule' },
  { path: 'picturePicker', loadChildren: './modals/picture-picker/picture-picker.module#PicturePickerPageModule' },
  { path: 'route-map', loadChildren: './route-map/route-map.module#RouteMapPageModule' }
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
