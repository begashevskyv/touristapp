import { NgModule } from '@angular/core';
import { TranslationComponent } from './translation/translation.component';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { RecentMemosComponent } from './recent-memos/recent-memos.component';

@NgModule({
  imports: [
    IonicModule,
    CommonModule],
  declarations: [TranslationComponent, RecentMemosComponent],
  exports: [TranslationComponent, RecentMemosComponent]
})
export class ComponentsModule { }
