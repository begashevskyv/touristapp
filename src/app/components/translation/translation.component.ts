import { Component, OnInit, Input } from '@angular/core';
import { Translation } from '../../models/translation';
import { LanguagesService } from '../../services/languages.service';
import { RouteMapNavigationService } from '../../services/navigation/route-map-navigation.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-translation',
  templateUrl: './translation.component.html',
  styleUrls: ['./translation.component.scss']
})
export class TranslationComponent implements OnInit {

  @Input() translation: Translation = null;

  public languagesMap: Map<string, string> = new Map<string, string>();

  constructor(
    private router: Router,
    private routeMapNav: RouteMapNavigationService,
    private languagesService: LanguagesService) { }

  ngOnInit() {
    this.languagesService.getCachedAsMap().subscribe(languages => {
      this.languagesMap = languages;
    });
  }

  public get from(): string {
    return this.languagesMap.get(this.translation.setup.from);
  }

  public get to(): string {
    return this.languagesMap.get(this.translation.setup.to);
  }

  public get text(): string {
    return this.translation.text;
  }

  public get translatedText(): string {
    return this.translation.translatedText;
  }

  public get locationName(): string {
    return this.translation.locationName;
  }

  public route() {
    this.routeMapNav.destination = this.translation.location;
    this.router.navigate(['/route-map']);
  }
}
